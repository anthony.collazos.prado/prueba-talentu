function mostrarUsuarios(listado) {
	var contenido = '<div class="table-responsive"><table class="table table-striped table-hover"><thead><tr><th scope="col"> </th><th scope="col">Id</th><th scope="col">Nombre</th><th scope="col">Edad</th><th scope="col">Email</th></tr></thead><tbody id="listaUsuarios">';
	for (var i = 0; i < listado.length; i++) {
		var numero = i + 1;
		contenido += '<tr><th scope="row" class="align-middle"><img src="' + listado[i]['avatar'] + '" class="rounded-circle" width="50" height="50" class="float-start"></th><td class="align-middle">' + listado[i]['id'] + '</td><td class="align-middle"><b>' + listado[i]['first_name'] + ' ' + listado[i]['last_name'] + '</b></td><td class="align-middle">25</td><td colspan="2" class="align-middle">' + listado[i]['email'] + '</td></tr>';
	}
	contenido += '</table></tbody></div>';
	document.getElementById('usuarios').innerHTML = contenido;
}

function mostrarDatos() {
	var contenido = '<div class="row">' +
		'<div class="col-4 d-none d-md-block">' +
			'<img src="./img/rdp_img1.jpg" class="img-fluid" alt="Registro de Personas">' +
		'</div>' +
		'<div class="col">' +
			'<h3>' +
				'<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16">' +
					'<path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>' +
					'<path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>' +
				'</svg> Agrega los datos' +
			'</h3>' +
			'<br>' +
			'<form class="row">' +
				'<div class="mb-3">' +
					'<label for="nombre" class="visually-hidden">Nombre(s)</label>' +
					'<input type="text" class="form-control" id="nombre" placeholder="Nombre(s)">' +
				'</div>' +
				'<div class="mb-3">' +
					'<label for="apellido" class="visually-hidden">Apellido(s)</label>' +
					'<input type="text" class="form-control" id="apellido" placeholder="Apellido(s)">' +
				'</div>' +
				'<div class="mb-3">' +
					'<label for="email" class="visually-hidden">Email</label>' +
					'<input type="text" class="form-control" id="email" placeholder="Email">' +
				'</div>' +
				'<div class="col">' +
					'<div class="row">' +
						'<div class="col-auto">' +
							'<label for="fechaNacimiento" class="col-form-label">Fecha de Nacimiento</label>' +
						'</div>' +
						'<div class="col">' +
							'<input id="fechaNacimiento" class="form-control" type="date">' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</form>' +
			'<br>' +
			'<button type="button" class="btn btn-primary alinear-derecha-2" id="boton-guardar" onclick="guardarUsuario(document.getElementById(\'listaUsuarios\'))">' +
				'<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-save2-fill" viewBox="0 0 16 16">' +
					'<path d="M8.5 1.5A1.5 1.5 0 0 1 10 0h4a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h6c-.314.418-.5.937-.5 1.5v6h-2a.5.5 0 0 0-.354.854l2.5 2.5a.5.5 0 0 0 .708 0l2.5-2.5A.5.5 0 0 0 10.5 7.5h-2v-6z"/>' +
				'</svg> Guardar' +
			'</button>' +
			'<button type="button" class="btn btn-dark me-3 alinear-derecha-2" id="boton-volver" onclick="quitarDatos()">' +
				'<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply-fill" viewBox="0 0 16 16">' +
					'<path d="M5.921 11.9 1.353 8.62a.719.719 0 0 1 0-1.238L5.921 4.1A.716.716 0 0 1 7 4.719V6c1.5 0 6 0 7 8-2.5-4.5-7-4-7-4v1.281c0 .56-.606.898-1.079.62z"/>' +
				'</svg> Volver' +
			'</button>' +
			'<div id="respuesta"></div>' +
		'</div>' +
	'</div>';
	document.getElementById('datos').innerHTML = contenido;
}

function quitarDatos() {
	var contenido = '<div class="row"><div class="col"><button type="button" class="btn btn-primary alinear-derecha" id="boton-agregar-usuario" onclick="mostrarDatos()"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16"><path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/><path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/></svg> Agregar Usuario</button><br></div></div>';
	document.getElementById('datos').innerHTML = contenido;
}

function guardarUsuario(elemento) {
	var nombre = document.getElementById('nombre').value;
	var apellido = document.getElementById('apellido').value;
	var email = document.getElementById('email').value;
	var fechaNacimiento = document.getElementById('fechaNacimiento').value;
	var edad = obtenerEdad(fechaNacimiento);
	var cantidad = elemento.getElementsByTagName('tr').length;
	var numero = cantidad + 1;
	var nodo = document.createElement('tr');
	var aleatorio = Math.round(Math.random()*6);
	if (validarCampos(nombre, apellido, email, fechaNacimiento)) {
		nodo.innerHTML = '<tr><th scope="row" class="align-middle"><img src="https://reqres.in/img/faces/' + aleatorio + '-image.jpg" class="rounded-circle" width="50" height="50" class="float-start"></th><td class="align-middle">' + numero + '</td><td class="align-middle"><b>' + nombre + ' ' + apellido + '</b></td><td class="align-middle">' + edad + '</td><td colspan="2" class="align-middle">' + email + '</td></tr>';
		elemento.appendChild(nodo);
		document.getElementById('respuesta').innerHTML = '<br><div class="alert alert-primary mt-5" role="alert"><b>Listo,</b> los datos se guardaron correctamente.</div>';
		document.getElementById('nombre').value = '';
		document.getElementById('apellido').value = '';
		document.getElementById('email').value = '';
		document.getElementById('fechaNacimiento').value = '';
	}
}

function validarCampos(nombre, apellido, email, fechaNacimiento) {
	var validacion = false;
	var validacionNombre1 = false;
	var validacionApellido1 = false;
	var validacionEmail1 = false;
	var validacionFechaNacimiento1 = false;
	var validacionNombre2 = false;
	var validacionApellido2 = false;
	var validacionEmail2 = false;

	if (nombre != '') {
		validacionNombre1 = true;
		if (validarTexto(nombre)) {
			validacionNombre2 = true;
		}
	}
	if (apellido != '') {
		validacionApellido1 = true;
		if (validarTexto(apellido)) {
			validacionApellido2 = true;
		}
	}
	if (email != '') {
		validacionEmail1 = true;
		if (validarCorreo(email)) {
			validacionEmail2 = true;
		}
	}
	if (fechaNacimiento != '') {
		validacionFechaNacimiento1 = true;
	}
	if (validacionNombre1 && validacionApellido1 && validacionEmail1 && validacionFechaNacimiento1) {
		if (!validacionNombre2) {
			document.getElementById('respuesta').innerHTML = '<br><div class="alert alert-danger mt-5" role="alert"><b>Error,</b> ese no es un nombre válido.</div>';
		}
		if (!validacionApellido2) {
			document.getElementById('respuesta').innerHTML = '<br><div class="alert alert-danger mt-5" role="alert"><b>Error,</b> ese no es un apellido válido.</div>';
		}
		if (!validacionEmail2) {
			document.getElementById('respuesta').innerHTML = '<br><div class="alert alert-danger mt-5" role="alert"><b>Error,</b> ese no es un correo válido.</div>';
		}
		if (validacionNombre2 && validacionApellido2 && validacionEmail2) {
			validacion = true;
		}
	} else {
		document.getElementById('respuesta').innerHTML = '<br><div class="alert alert-danger mt-5" role="alert"><b>Error,</b> es necesario llenar todos los campos.</div>';
	}
	return validacion;
}

function validarCorreo(valor) {
	var correoRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	var respuesta = correoRegex.test(valor);
	return respuesta;
}

function validarTexto(valor) {
	var textoRegex = /^[a-zA-Z\s]*$/g;
	var respuesta = textoRegex.test(valor);
	return respuesta;
}

function obtenerEdad(fechaNacimiento) {
	var array = fechaNacimiento.split('-');
	var d1 = array[2];
	var m1 = array[1];
	var y1 = array[0];  
	var date = new Date();
	var d2 = date.getDate();
	var m2 = 1 + date.getMonth();
	var y2 = date.getFullYear();
	var month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];  
	if(d1 > d2){
		d2 = d2 + month[m2 - 1];
		m2 = m2 - 1;
	}
	if(m1 > m2){
		m2 = m2 + 12;
		y2 = y2 - 1;
	}
	var d = d2 - d1;
	var m = m2 - m1;
	var y = y2 - y1;
	return y;
}