<?php 
    $respuesta = file_get_contents('https://reqres.in/api/users?page=1');
    $arreglo = json_decode($respuesta, true);
    $data = $arreglo['data'];
    $final = json_encode($data);
?>

<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/style.css">
        <script src="js/script.js?v=2"></script>
        <title>Prueba Técnica Talentu</title>
    </head>
    <body class="fondo-celeste">
        <nav class="navbar navbar-expand-md navbar-dark fondo-azul mb-4">
            <div class="container contenedor">
                <img class="ms-3" src="./img/talentuColorBlanco-2.png" width="100" class="img-fluid" alt="Registro de Personas">
                <a class="navbar-brand mediano" href="#">Prueba Técnica</a>
            </div>
        </nav>
        <main class="container my-3 contenedor">
            <div class="bg-white p-4 rounded">
                <br>
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 15 20">
                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                        <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"></path>
                        <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"></path>
                    </svg> Lista de Usuarios
                </h2>
                <br>
                <hr>
                <div id="usuarios"></div>
                <br>
                <div id="datos">
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-primary alinear-derecha" id="boton-agregar-usuario" onclick="mostrarDatos()">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16">
                                    <path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                    <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                                </svg> Agregar Usuario
                            </button>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script type="text/javascript">
            var listaFinal = JSON.parse('<?php echo $final; ?>');
            mostrarUsuarios(listaFinal);
        </script>
    </body>
</html>
